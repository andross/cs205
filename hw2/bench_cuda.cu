#include <stdio.h>
#include "cublas_v2.h"

#define INDEX(row, col, len) (((col)*(len)) + (row))
#define TILE 32
#define VERBOSE 0
#define GFLOPSs(n, t) (2.0 * n * n * n / (t / 1000.0) * 1e-9)

__global__ void naive_matmul(size_t n, float* A, float* B, float* C) {
  const size_t x = blockDim.x*blockIdx.x + threadIdx.x;
  const size_t y = blockDim.y*blockIdx.y + threadIdx.y;

  register float c = 0.0;

  for (size_t k = 0; k < n; k++)
    c += A[INDEX(x,k,n)] * B[INDEX(k,y,n)];

  C[INDEX(x,y,n)] = c;
}

__global__ void tiled_matmul(size_t n, float* A, float* B, float* C) {
  const size_t tx = threadIdx.x;
  const size_t ty = threadIdx.y;
  const size_t x = blockDim.x*blockIdx.x + tx;
  const size_t y = blockDim.y*blockIdx.y + ty;
  __shared__ float Ash[TILE][TILE+1];
  __shared__ float Bsh[TILE][TILE+1];

  register float c = 0.0;

  for (size_t t = 0; t < n/TILE; t += 1) {
    Ash[tx][ty] = A[INDEX(x,ty,n) + t*TILE*n];
    Bsh[tx][ty] = B[INDEX(tx,y,n) + t*TILE];
    __syncthreads();

    for (size_t k = 0; k < TILE; k++)
      c += Ash[tx][k] * Bsh[k][ty];
    __syncthreads();
  }

  C[INDEX(x,y,n)] = c;
}

int main(int argc, char *argv[]) {
  const size_t n = (size_t)atoi(argv[1]);
  const size_t bytes = n * n * sizeof(float);
  if (VERBOSE)
    printf("Total memory required is %lf MB\n", 3.0 * (double)bytes / 1000000.0);

  float t_cublas, t_naive, t_tiled;
  dim3 threads(TILE, TILE, 1);
  dim3 blocks(n/TILE, n/TILE, 1);

  // Initialize host
  float* hA = (float*)malloc(bytes);
  float* hB = (float*)malloc(bytes);
  float* hC = (float*)malloc(bytes);
  for (size_t i = 0; i < n*n; i++) {
    hA[i] = double(rand()) / (double(RAND_MAX) + 1.0);
    hB[i] = double(rand()) / (double(RAND_MAX) + 1.0);
  }
  memset(hC, 0, bytes);

  // Initialize device
  float *dA, *dB, *dC;
  cudaMalloc((void**)&dA, bytes);
  cudaMalloc((void**)&dB, bytes);
  cudaMalloc((void**)&dC, bytes);
  cudaMemcpy(dA, hA, bytes, cudaMemcpyHostToDevice);
  cudaMemcpy(dB, hA, bytes, cudaMemcpyHostToDevice);
  cudaMemset(dC, 0, bytes);

  cublasHandle_t handle;
  cublasCreate(&handle);
  float alpha = 1.0;
  float beta  = 0.0;

  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  cudaEventRecord(start, 0);
  cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N,
              (int)n, (int)n, (int)n, &alpha,
              dA, (int)n,
              dB, (int)n, &beta,
              dC, (int)n);
  cudaEventRecord(stop, 0);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&t_cublas, start, stop);
  cudaMemcpy(hC, dC, bytes, cudaMemcpyDeviceToHost);
  float cublas_val = hC[0];
  cudaMemset(dC, 0, bytes);

  cudaEventRecord(start, 0);
  tiled_matmul<<<blocks, threads>>>(n, dA, dB, dC);
  cudaEventRecord(stop, 0);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&t_tiled, start, stop);
  cudaMemcpy(hC, dC, bytes, cudaMemcpyDeviceToHost);
  float tiled_val = hC[0];
  cudaMemset(dC, 0, bytes);

  cudaEventRecord(start, 0);
  naive_matmul<<<blocks, threads>>>(n, dA, dB, dC);
  cudaEventRecord(stop, 0);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&t_naive, start, stop);
  cudaMemcpy(hC, dC, bytes, cudaMemcpyDeviceToHost);
  float naive_val = hC[0];
  cudaMemset(dC, 0, bytes);

  if (VERBOSE)
    printf("results are %f, %f, %f\n", cublas_val, tiled_val, naive_val);

  printf("%zu,%f,%f,%f\n", n, GFLOPSs(n, t_cublas), GFLOPSs(n, t_naive), GFLOPSs(n, t_tiled));

  cublasDestroy(handle);
  cudaEventDestroy(start);
  cudaEventDestroy(stop);
  cudaFree(dA);
  cudaFree(dB);
  cudaFree(dC);
  free(hA);
  free(hB);
  free(hC);
  cudaDeviceReset();

  return 0;
}
