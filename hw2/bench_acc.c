#include <stdlib.h>
#include <stdio.h>
#include <openacc.h>
#include <sys/time.h>
#define MIN(a,b) (a < b) ? a : b
#define VERBOSE 0
#define GFLOPSs(n, t) (2.0 * n * n * n / (t / 1000.0) * 1e-9)

struct timeval t_start, t_stop, t_spent;

void set_timer() {
  gettimeofday(&t_start, NULL);
}

double get_timer() {
  gettimeofday(&t_stop, NULL);
  timersub(&t_stop, &t_start, &t_spent);
  return 1000.0 * t_spent.tv_sec + t_spent.tv_usec / 1000.0;
}

void matmul_acc(size_t n, float* A, float* B, float* C) {
  size_t i, j, k;
  #pragma acc parallel loop private(i,j,k) tile(32,16) copyin(A[:n*n], B[:n*n]) copyout(C[:n*n])
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      float sum = 0;
      #pragma acc loop seq
      for (k = 0; k < n; k++) {
        sum += A[i*n + k] * B[k*n + j];
      }
      C[i*n + j] = sum;
    }
  }
}

int main(int argc, char *argv[]) {
  const size_t n = (size_t)atoi(argv[1]);
  const size_t bytes = n * n * sizeof(float);
  if (VERBOSE)
    printf("Total memory required is %lf MB\n", 3.0 * (double)bytes / 1000000.0);

  float* A = (float*)malloc(bytes);
  float* B = (float*)malloc(bytes);
  float* C = (float*)malloc(bytes);
  for (size_t i = 0; i < n; i++) {
    for (size_t j = 0; j < n; j++) {
      A[i*n+j] = 1;
      B[i*n+j] = 1;
    }
  }
  memset(C, 0, bytes);

  set_timer();
  matmul_acc(n, A, B, C);
  double t_acc_init = get_timer();
  if (VERBOSE) printf("%f should eq %f should eq %zu\n", C[0], C[n*n-1], n);

  memset(C, 0, bytes);
  if (VERBOSE) printf("%f should eq %f should eq 0\n", C[0], C[n*n-1]);

  set_timer();
  matmul_acc(n, A, B, C);
  double t_acc = get_timer();
  if (VERBOSE) printf("%f should eq %f should eq %zu\n", C[0], C[n*n-1], n);

  printf("%zu,%f\n", n, GFLOPSs(n, t_acc));

  free(A);
  free(B);
  free(C);

  return 0;
}
