\documentclass[12pt]{article}
\usepackage{fullpage,amsmath,amsfonts,mathpazo,microtype,nicefrac,graphicx}
\usepackage{listings}
\usepackage{mathtools}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
% Set-up for hypertext references
\usepackage{hyperref,color,textcomp}
\definecolor{webgreen}{rgb}{0,.35,0}
\definecolor{webbrown}{rgb}{.6,0,0}
\definecolor{RoyalBlue}{rgb}{0,0,0.9}
\hypersetup{
   colorlinks=true, linktocpage=true, pdfstartpage=3, pdfstartview=FitV,
   breaklinks=true, pdfpagemode=UseNone, pageanchor=true, pdfpagemode=UseOutlines,
   plainpages=false, bookmarksnumbered, bookmarksopen=true, bookmarksopenlevel=1,
   hypertexnames=true, pdfhighlight=/O,
   urlcolor=webbrown, linkcolor=RoyalBlue, citecolor=webgreen,
   pdfauthor={Andrew Ross},
   pdfsubject={Stat 110},
   pdfkeywords={},
   pdfcreator={pdfLaTeX},
   pdfproducer={LaTeX with hyperref}
}
\setlength\parindent{0pt}
%\lstset{basicstyle=\ttfamily\footnotesize,breaklines=true}
\newcommand*{\ttfamilywithbold}{\fontfamily{lmtt}\selectfont}

\begin{document}

\section*{CS205 Homework 2, Andrew Ross}

\subsection*{Analysis of Parallel Algorithms}

1. The isoefficiency function of a parallel algorithm is a relationship between the number of units of work, $W$, and the required number of processors $p$ to maintain a \textit{fixed} efficiency $E$ (which is equal to the speedup over $p$). In an ideally scalable parallel system with no overheads of any kind in adding processors and no interdependencies, then we can assign each unit of work to its own processor and achieve the same efficiency (of 1). In that case, $W=p$. Even if each processor has to do more than one unit of work, if the relationship between $W$ and $p$ is still linear, i.e. $W = \Theta(p)$, we still call the system ideally scalable. \newline

2. If we consider the logarithmic summation algorithm in the case where $n=p$, we have $\log(n)$ steps in time, each of which require 1 time unit for addition and 20 time units for communication, so the algorithm requires $21\log(n)$ units of time (as opposed to $n$ units for time for the sequential program). Clearly, this is only advantageous for very large $n$. If we let $p$ differ, then I believe we spend $\frac{n}{p}$ units of time summing independent chunks of the array followed by $21\log(p)$ units of time merging them, for a total of $\frac{n}{p} + 21\log(p)$. We can now plot speedup and scaled speedup curves:

\begin{center}
\includegraphics[width=0.5\textwidth]{scaled-speedup.png}
\end{center}

For a fixed problem size $n$, normal speedup reaches a maximum near $p=8$, and scaled speedup is a bit lower than ideal but still linear. If we reduce the communication overhead to 0, the normal speedup is maximized at a much higher value of $p$, and the scaled speedup is much closer though not quite as good as the ideal.

\subsection*{Parallel Matrix Algorithms in CUDA and OpenAcc}

\subsubsection*{CUDA}

The basic 3-loop matrix multiplication is very easy to parallelize with CUDA, since
we just implement the innermost loop as a kernel:

\begin{lstlisting}[language=C,basicstyle=\ttfamilywithbold\footnotesize]
#define idx(row, col, len) (((col)*(len)) + (row))

__global__ void naive_mm(size_t n, float* A, float* B, float* C) {
  const size_t x = blockDim.x*blockIdx.x + threadIdx.x;
  const size_t y = blockDim.y*blockIdx.y + threadIdx.y;
  register float c = 0.0;

  for (size_t k = 0; k < n; k++)
    c += A[idx(x,k,n)] * B[idx(k,y,n)];

  C[idx(x,y,n)] = c;
}
\end{lstlisting}

Each thread computes exactly one of the $n^2$ dot products, then updates the result.
However, this approach incurs unnecessarily large data movement costs, since the same sections
of the matrix are read multiple times to different thread blocks. We can alleviate this
at least somewhat using tiling:

\begin{lstlisting}[language=C,basicstyle=\ttfamilywithbold\footnotesize]
#define TILE 32

__global__ void tiled_mm(size_t n, float* A, float* B, float* C) {
  const size_t tx = threadIdx.x;
  const size_t ty = threadIdx.y;
  const size_t x = blockDim.x*blockIdx.x + tx;
  const size_t y = blockDim.y*blockIdx.y + ty;
  __shared__ float Ash[TILE][TILE+1];
  __shared__ float Bsh[TILE][TILE+1];
  register float c = 0.0;

  for (size_t t = 0; t < n/TILE; t += 1) {
    Ash[tx][ty] = A[idx(x,ty,n) + t*TILE*n];
    Bsh[tx][ty] = B[idx(tx,y,n) + t*TILE];
    __syncthreads();

    for (size_t k = 0; k < TILE; k++)
      c += Ash[tx][k] * Bsh[k][ty];
    __syncthreads();
  }

  C[idx(x,y,n)] = c;
}
\end{lstlisting}

Where we first read a section of the matrix into the shared memory of a thread block (in parallel), then have
each thread within that block compute the partial dot product. Each thread still corresponds to exactly
one dot product, but it computes this dot product in multiple stages. A tile size of 32 (which for single-precision floats corresponds to a total memory size of $3*4*32^2$ B $\approx$ 12.5 KB) performed best in practice.  \newline

Performance-wise, the tiled approach beats the naive approach by about a factor of two, but both methods
underperform CUBLAS, which is highly optimized and achieves nearly the peak performance possible on the Tesla K20Xm GPU the algorithms were tested on:

\begin{center}
\includegraphics[width=0.67\textwidth]{gemm-cuda.png}
\end{center}

\subsubsection*{OpenAcc}

My initial strategy for implementing fast matrix multiplication in OpenAcc was to
start with the following \textit{sequential} tiled matrix multiplication routine:

\begin{lstlisting}[language=C,basicstyle=\ttfamilywithbold\footnotesize]
void seq_tiled_mm(size_t n, float* A, float* B, float* C) {
  for (size_t ii = 0; ii < n; ii+=TILE) {
    for (size_t jj = 0; jj < n; jj+=TILE) {
      for (size_t kk = 0; kk < n; kk+=TILE) {
        for (size_t i=ii; i<ii+TILE; i++) {
          for (size_t j=jj; j<jj+TILE; j++) {
            float c = 0;
            for(size_t k=kk; k<kk+TILE; k++)
              c += A[idx(i,k,n)] * B[idx(k,j,n)];
            C[idx(i,j,n)] += c;
          }
        }
      }
    }
  }
}
\end{lstlisting}

and parallelize it using \texttt{\#pragma}s. However, despite a fairly thorough exploration of the search
space of possible \texttt{\#pragma} placements, I ended up achieving the best performance with a parallelized
version of the naive 3-loop routine:

\begin{lstlisting}[language=C,basicstyle=\ttfamilywithbold\footnotesize]
void openacc_mm(size_t n, float* A, float* B, float* C) {
  size_t i, j, k;
  #pragma acc parallel loop private(i,j,k) tile(32,16) \
    copyin(A[:n*n], B[:n*n]) copyout(C[:n*n])
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      float c = 0;
      #pragma acc loop seq
      for (k = 0; k < n; k++)
        c += A[idx(i,k,n)] * B[idx(k,j,n)];
      C[idx(i,j,n)] = c;
    }
  }
}
\end{lstlisting}

After a fair amount of experimentation with \texttt{gang}, \texttt{vector}, and \texttt{tile}, using \texttt{tile(32,16)} gave me the best results. Even so, performance was worse than any of the CUDA results, only hovering near 100 GFlop/s:

\begin{center}
\includegraphics[width=0.67\textwidth]{gemm-acc.png}
\end{center}

Perhaps part of the difficulty I had optimizing in OpenAcc is the answer to the final question, which is the size of the search space. In the six loop version I initially tried to optimize, even if we only pick two loops to parallelize, there are still ${6 \choose 2}=15$ possible loops (which, in general, if there are $l$ loops and we can pick two is $\Theta(l^2)$ in the number of loops), for which we can choose many gang, vector, and tile configurations. Just limiting to gang and vector sizes, if there are $k$ possible choices given the size of $n$, we have an $\Theta(k^2)$ number of size options -- so we're exploring a search space that is at least $\Theta(l^2k^2)$. Exploring all of these possibilities is an impossibility, so ideally we should either develop mathematical methods that identify the best configuration in theory or optimization methods that can find it in practice. \newline

I tested my algorithms both using random floats and matrices full of 1s and confirmed that values at different locations matched (between implementations for random floats and matched $n$ for 1s). Full code is available at \href{https://bitbucket.org/andross/cs205/src/master/hw2}{bitbucket.org/andross/cs205/src/master/hw2}.

\end{document}

