#include <stdlib.h>
#include <omp.h>
#include <gsl/gsl_blas.h>
#define MIN(a,b) (a < b) ? a : b
#define res double

res seq_vector_sum(res* array, size_t length) {
  res total = 0;
  for (size_t i = 0; i < length; i++)
    total += array[i];
  return total;
}

res fast_vector_sum(res* array, size_t length) {
  res total = 0;
  #pragma omp parallel for reduction(+:total)
  for (size_t i = 0; i < length; i++)
    total += array[i];
  return total;
}

res cheap_vector_sum(res* array, size_t length, size_t p) {
  res partials[p];
  size_t step = length / p + (length % p != 0);
  #pragma omp parallel for
  for (size_t thread = 0; thread < p; thread++) {
    res partial = 0;
    size_t a = step*thread;
    size_t b = MIN(length, step*(thread+1));
    for (size_t i = a; i < b; i++)
      partial += array[i];
    partials[thread] = partial;
  }
  #pragma omp barrier
  res total = 0;
  #pragma omp parallel for reduction(+:total)
  for (size_t i = 0; i < p; i++) {
    total += partials[i];
  }
  return total;
}

void seq_matvec_mult(res* matrix, res* vector, res* result, size_t length) {
  for (size_t i = 0; i < length; i++) {
    res total = 0;
    for (size_t j = 0; j < length; j++)
      total += matrix[i*length + j] * vector[j];
    result[i] = total;
  }
}

void fast_matvec_mult1(res* matrix, res* vector, res* result, size_t length) {
  #pragma omp parallel for
  for (size_t i = 0; i < length; i++) {
    res total = 0;
    for (size_t j = 0; j < length; j++)
      total += matrix[i*length + j] * vector[j];
    result[i] = total;
  }
}

//void fast_matvec_mult2(res* matrix, res* vector, res* result, size_t length) {
  //for (size_t i = 0; i < length; i++) {
    //res total = 0;
    //#pragma omp parallel for reduction(+:total)
    //for (size_t j = 0; j < length; j++)
      //total += matrix[i*length + j] * vector[j];
    //result[i] = total;
  //}
//}

void fast_matvec_mult2(res* matrix, res* vector, res* result, size_t length) {
  #pragma omp parallel for
  for (size_t i = 0; i < length; i++)
    result[i] = 0;
  #pragma omp parallel for collapse(2)
  for (size_t i = 0; i < length; i++)
    for (size_t j = 0; j < length; j++)
      result[i] += matrix[i*length + j] * vector[j];
}

void cheap_matvec_mult(res* matrix, res* vector, res* result, size_t length, size_t p) {
  res partials[p];
  size_t step = length / p + (length % p != 0);
  for (size_t i = 0; i < length; i++) {
    #pragma omp parallel for
    for (size_t thread = 0; thread < p; thread++) {
      res partial = 0;
      size_t a = step*thread;
      size_t b = MIN(length, step*(thread+1));
      for (size_t j = a; j < b; j++)
        partial += matrix[i*length + j] * vector[j];
      partials[thread] = partial;
    }
    #pragma omp barrier
    res total = 0;
    #pragma omp parallel for reduction(+:total)
    for (size_t i = 0; i < p; i++)
      total += partials[i];
    #pragma omp barrier
    result[i] = total;
  }
}

void seq_matmat_mult(res* matrix1, res* matrix2, res* result, size_t length) {
  for (size_t i = 0; i < length; i++) {
    for (size_t j = 0; j < length; j++) {
      result[i*length + j] = 0;
      for (size_t k = 0; k < length; k++)
        result[i*length + j] += matrix1[i*length + k] * matrix2[k*length + i];
    }
  }
}

void dgemm_matmat_mult(res* matrix1, res* matrix2, res* result, size_t length) {
   gsl_matrix_view A = gsl_matrix_view_array(matrix1, length, length);
   gsl_matrix_view B = gsl_matrix_view_array(matrix2, length, length);
   gsl_matrix_view C = gsl_matrix_view_array(result, length, length);
   gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
                  1.0, &A.matrix, &B.matrix,
                  0.0, &C.matrix);
}

void par_matmat_mult1(res* matrix1, res* matrix2, res* result, size_t length) {
  #pragma omp parallel for
  for (size_t i = 0; i < length; i++) {
    for (size_t j = 0; j < length; j++) {
      result[i*length + j] = 0;
      for (size_t k = 0; k < length; k++)
        result[i*length + j] += matrix1[i*length + k] * matrix2[k*length + j];
    }
  }
}

void par_matmat_mult2(res* matrix1, res* matrix2, res* result, size_t length) {
  #pragma omp parallel for collapse(2)
  for (size_t i = 0; i < length; i++) {
    for (size_t j = 0; j < length; j++) {
      result[i*length + j] = 0;
      for (size_t k = 0; k < length; k++)
        result[i*length + j] += matrix1[i*length + k] * matrix2[k*length + j];
    }
  }
}

void par_matmat_mult3(res* matrix1, res* matrix2, res* result, size_t length) {
  #pragma omp parallel for collapse(2)
  for (size_t i = 0; i < length; i++) {
    for (size_t j = 0; j < length; j++) {
      res sum = 0;
      #pragma omp parallel for reduction(+:sum)
      for (size_t k = 0; k < length; k++)
        sum += matrix1[i*length + k] * matrix2[k*length + j];
      result[i*length + j] = sum;
    }
  }
}

void tiled_seq_matmat_mult(res* matrix1, res* matrix2, res* result, size_t length, size_t b) {
  for (size_t ii = 0; ii < length; ii+=b) {
    for (size_t jj = 0; jj < length; jj+=b) {
      for (size_t kk = 0; kk < length; kk+=b) {
        size_t imax = MIN(ii+b, length);
        size_t jmax = MIN(jj+b, length);
        size_t kmax = MIN(kk+b, length);
        for (size_t i=ii; i<imax; i++) {
          for (size_t j=jj; j<jmax; j++) {
            for(size_t k=kk; k<kmax; k++) {
              result[i*length + j] += matrix1[i*length + k] * matrix2[k*length + j];
            }
          }
        }
      }
    }
  }
}

void tiled_par_matmat_mult1(res* matrix1, res* matrix2, res* result, size_t length, size_t b) {
  #pragma omp parallel for collapse(2)
  for (size_t ii = 0; ii < length; ii+=b) {
    for (size_t jj = 0; jj < length; jj+=b) {
      for (size_t kk = 0; kk < length; kk+=b) {
        size_t imax = MIN(ii+b, length);
        size_t jmax = MIN(jj+b, length);
        size_t kmax = MIN(kk+b, length);
        for (size_t i=ii; i<imax; i++) {
          for (size_t j=jj; j<jmax; j++) {
            for(size_t k=kk; k<kmax; k++) {
              result[i*length + j] += matrix1[i*length + k] * matrix2[k*length + j];
            }
          }
        }
      }
    }
  }
}
