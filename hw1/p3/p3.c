#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include "implementations.c"

int get_time() {
  struct timeval t;
  gettimeofday(&t, NULL);
  int tres = t.tv_usec;
  tres += t.tv_sec * 1000000;
  return tres;
}

#define BENCHMARK(t, block) int t; do {\
  int t1 = get_time();\
  block;\
  int t2 = get_time();\
  t = (t2 - t1);\
} while(0);

#define ASSERT_EQUAL(v1, v2) do {\
  if (v1 != v2) {\
    printf("Assertion error\n");\
    return 1;\
  }\
} while(0);

int main(int argc, char** argv) {
  //size_t iters = 10;
  size_t length = (size_t)atoi(argv[1]);
  int algo = atoi(argv[2]);
  size_t p = (size_t)atoi(argv[3]);

  if (algo == 0) {
    //
    // SUMMATION
    //
    size_t sum = 0;
    res* vector = (res*)malloc(length * sizeof(res));
    for (size_t i = 0; i < length; i++)
      vector[i] = 1;

    BENCHMARK(t_seq, { sum = seq_vector_sum(vector, length); });
    ASSERT_EQUAL(sum, length);

    BENCHMARK(t_par1, { sum = fast_vector_sum(vector, length); });
    ASSERT_EQUAL(sum, length);

    BENCHMARK(t_par2, { sum = cheap_vector_sum(vector, length, p); });
    ASSERT_EQUAL(sum, length);

    printf("%d,%d,%d,%d\n", (int)length, t_seq, t_par1, t_par2);
  } else if (algo == 1) {
    //
    // MATRIX-VECTOR
    //
    res* matrix = (res*)malloc(length * length * sizeof(res));
    res* vector = (res*)malloc(length * sizeof(res));
    res* result = (res*)malloc(length * sizeof(res));
    for (size_t i = 0; i < length; i++) {
      vector[i] = 1;
      for (size_t j = 0; j < length; j++)
        matrix[i*length + j] = 1;
    }

    BENCHMARK(t_seq, { seq_matvec_mult(matrix, vector, result, length); });
    ASSERT_EQUAL(result[0], length);

    BENCHMARK(t_par1, { fast_matvec_mult1(matrix, vector, result, length); });
    ASSERT_EQUAL(result[0], length);

    BENCHMARK(t_par2, { fast_matvec_mult2(matrix, vector, result, length); });
    ASSERT_EQUAL(result[0], length);

    printf("%d,%d,%d,%d\n", (int)length, t_seq, t_par1, t_par2);
  } else {
    //
    // MATRIX-MATRIX
    //
    res* matrix1 = (res*)malloc(length * length * sizeof(res));
    res* matrix2 = (res*)malloc(length * length * sizeof(res));
    res* result = (res*)malloc(length * length * sizeof(res));
    for (size_t i = 0; i < length; i++) {
      for (size_t j = 0; j < length; j++) {
        matrix1[i*length + j] = 1;
        matrix2[i*length + j] = 1;
      }
    }
    #define CLEAR_RESULT() do {\
    for (size_t i = 0; i < length; i++)\
      for (size_t j = 0; j < length; j++)\
        result[i*length + j] = 0;\
    } while(0);

    //size_t b = MIN(length, 256);
    size_t b = p;

    // warm the cache
    seq_matmat_mult(matrix1, matrix2, result, length);
    CLEAR_RESULT();

    BENCHMARK(t_seq, { seq_matmat_mult(matrix1, matrix2, result, length); });
    ASSERT_EQUAL(result[0], length);
    CLEAR_RESULT();

    BENCHMARK(t_dgemm, { dgemm_matmat_mult(matrix1, matrix2, result, length); });
    ASSERT_EQUAL(result[0], length);
    CLEAR_RESULT();

    BENCHMARK(t_par1, { par_matmat_mult1(matrix1, matrix2, result, length); });
    ASSERT_EQUAL(result[0], length);
    CLEAR_RESULT();

    BENCHMARK(t_par2, { par_matmat_mult2(matrix1, matrix2, result, length); });
    ASSERT_EQUAL(result[0], length);
    CLEAR_RESULT();

    BENCHMARK(t_par3, { par_matmat_mult3(matrix1, matrix2, result, length); });
    ASSERT_EQUAL(result[0], length);
    CLEAR_RESULT();

    BENCHMARK(t_stile, { tiled_seq_matmat_mult(matrix1, matrix2, result, length, b); });
    ASSERT_EQUAL(result[0], length);
    CLEAR_RESULT();

    BENCHMARK(t_ptile1, { tiled_par_matmat_mult1(matrix1, matrix2, result, length, b); });
    ASSERT_EQUAL(result[0], length);
    CLEAR_RESULT();

    printf("%d,%d,%d,%d,%d,%d,%d,%d\n", (int)length, t_seq, t_dgemm, t_par1, t_par2, t_par3, t_stile, t_ptile1);
  }
  return 0;
}
