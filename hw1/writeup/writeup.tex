\documentclass[12pt]{article}
\usepackage{fullpage,amsmath,amsfonts,mathpazo,microtype,nicefrac,graphicx}
\usepackage{listings}
\usepackage{mathtools}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
% Set-up for hypertext references
\usepackage{hyperref,color,textcomp}
\definecolor{webgreen}{rgb}{0,.35,0}
\definecolor{webbrown}{rgb}{.6,0,0}
\definecolor{RoyalBlue}{rgb}{0,0,0.9}
\hypersetup{
   colorlinks=true, linktocpage=true, pdfstartpage=3, pdfstartview=FitV,
   breaklinks=true, pdfpagemode=UseNone, pageanchor=true, pdfpagemode=UseOutlines,
   plainpages=false, bookmarksnumbered, bookmarksopen=true, bookmarksopenlevel=1,
   hypertexnames=true, pdfhighlight=/O,
   urlcolor=webbrown, linkcolor=RoyalBlue, citecolor=webgreen,
   pdfauthor={Andrew Ross},
   pdfsubject={Stat 110},
   pdfkeywords={},
   pdfcreator={pdfLaTeX},
   pdfproducer={LaTeX with hyperref}
}
\setlength\parindent{0pt}

\begin{document}

\section*{CS205 Homework 1, Andrew Ross}

\subsection*{Analysis of Parallel Algorithms}

\begin{center}
\includegraphics[width=0.75\textwidth]{diagram.png}
\end{center}

\begin{table}[htb]
\centering
\begin{tabular}{c|c|c|c}
    & a) & b) & c) \\
\hline
1. Max deg. of concurrency & 8 & 8 & 8 \\
\hline
2. Critical path length  & 8 & 7 & 8 \\
\hline
3. Min $p$ for max speedup & 2 & 2 & 8 \\
\hline
4i. Max speedup, $p=2$ & 2 & 2 & 36/19 \\
\hline
4ii. Max speedup, $p=5$ & 2 & 2 & 36/10 \\
\end{tabular}
\label{tab:lime-vs-grad-performance}
\end{table}

For 1., the maximum degree of concurrency is always the number of leaf nodes in the computation graph. In practice we may never execute 8 tasks concurrently especially for a) and b), but in theory if we did all parent tasks first, we could. \\

For 2., the critical path length is just the longest path we can take from the start node to a leaf. \\

For 3., in a) one processor can follow the long path while the other processes the remaining leaves, in b) one processor can proceed down the longest path while the other processes hanging leaves as they are ready (which might make it slower in practice than a), and in c), although we can only use one processor at the beginning, as we travel down we can use the rest. \\

For 4., because the max speedup is achieved with 2 processors that execute equal numbers of tasks and finish at the same time, the max speedup for a) and b) is 2 by symmetry. For c) (i), with 2 processors, we have to do the first layer in 1 time step, then split up the remaining 35 into chunks of 2, which takes 18 steps. So the speedup is 36/(18+1). For c) (ii), with 5 processors, we do the first 5 layers in 5 time steps, then have 21 nodes remaining, which we can complete in another 5 time steps (almost 4). So the speedup should be 36/(5+5).

\subsection*{Summation and Matrix-Vector Multiplication}

\subsubsection*{Parallel Summation}

The basic summation algorithm just reduces values using single \texttt{for} loop over the $n$ elements of our input. This requires $T_1=n$ units of time. The time optimal summation algorithm uses $p=n$ processors to read each input element, then a logarithmic reduction process whereby pairs of values are merged, resulting in $T_n=\log(n)$ units of time, for a cost of $n\log(n)$. The cost-optimal version using $p$ processors breaks the inputs into chunks of size $\ceil{n/p}$, sums those chunks individually, then reduces them logarithmically (requiring $log(p)$ steps). So the total time required is \ceil{n/p} + $\log(p)$, and the cost is $n+p\log(p)$. \\

Using OpenMP, I implemented three versions of summation:

\begin{lstlisting}[language=C]
double seq_vector_sum(double* v, size_t n) {
  double total = 0;
  for (size_t i = 0; i < n; i++)
    total += v[i];
  return total;
}

double par_vector_sum(double* v, size_t n) {
  double total = 0;
  #pragma omp parallel for reduction(+:total)
  for (size_t i = 0; i < n; i++)
    total += v[i];
  return total;
}

#define min(a,b) (a < b) ? a : b

double cheap_vector_sum(double* v, size_t n, size_t p) {
  double partials[p];
  size_t step = n / p + (n % p != 0);
  #pragma omp parallel for
  for (size_t thread = 0; thread < p; thread++) {
    res partial = 0;
    size_t a = step*thread;
    size_t b = min(step*(thread+1), n);
    for (size_t i = a; i < b; i++)
      partial += v[i];
    partials[thread] = partial;
  }
  #pragma omp barrier
  double total = 0;
  #pragma omp parallel for reduction(+:total)
  for (size_t i = 0; i < p; i++)
    total += partials[i];
  return total;
}
\end{lstlisting}
\ \\
The \texttt{cheap\_vector\_sum} was my attempt to directly implement the cost-optimal version, but \texttt{par\_vector\_sum} does almost as well (see Odyssey plots below). I this is because OpenMP is extremely smart and already effectively implements cost-optimal summation given a \texttt{reduction(+)} declaration. There is one dramatic difference at $2^{20}$ which might reflect a difference in OpenMP's automatic behavior, though it could also be random.

\begin{center}
\includegraphics[width=\textwidth]{summation-results.png}
\end{center}

These plots do demonstrate the $n/p$ asymptotic execution time, though the overheads mean that the parallel algorithm is slower than the sequential algorithm until we reach fairly large vector sizes.

\subsubsection*{Matrix-Vector Multiplication}
 
 Let's now consider matrix-vector multiplication. Given an $n \times n$ matrix and a length-$n$ vector, in the sequential case we need to do $n$ additions and $n$ multiplications per row, leading to a total of $T_1=2n^2$ operations. If we parallelize the computation by doing each dot product in parallel, then we can reduce the problem size to $T_n=2n$ if $n=p$, though our cost is still $2n^2$. We can also consider applying our parallel summation to each dot product, which would give us an execution time of $2n^2/p + \log(p)$, and a cost of $2n^2 + \log(p)$, which is actually both slower and more costly than just doing rows in parallel. If we did both row parallelization and parallel summation, we would need $np$ threads and could cut execution time to $2n/p + \log(p)$, but this would definitely not be cost-optimal. \\
 
 Given this discussion, it actually seems like the cost-optimal approach is to parallelize the dot product computations. I also tried implementing parallel summation within the dot products, but because we're limited to much smaller values of $n$ (since we must fit an $n \times n$ matrix in memory), parallel summation was slower than sequential summation, and the extra factor of $n$ made its performance even worse. Here is code and Odyssey results for this approach:
 
\begin{lstlisting}[language=C]
void seq_matvec_mult(double* A, double* v, double* r, size_t n) {
  for (size_t i = 0; i < n; i++) {
    double total = 0;
    for (size_t j = 0; j < n; j++)
      total += A[i*n + j] * v[j];
    r[i] = total;
  }
}
void par_matvec_mult(double* A, double* v, double* r, size_t n) {
  #pragma omp parallel for
  for (size_t i = 0; i < n; i++) {
    double total = 0;
    for (size_t j = 0; j < n; j++)
      total += A[i*n + j] * v[j];
    r[i] = total;
  }
}
\end{lstlisting}

Speedup results show that we only start seeing the benefits of parallelization for large $n$:

\begin{center}
\includegraphics[width=\textwidth]{matvec-parallel-rows.png}
\end{center}

\pagebreak
\subsection*{Matrix-Matrix Multiplication}

The naive sequential algorithm for matrix-matrix multiplication requires $O(n^3)$ operations:

\begin{lstlisting}[language=C]
void seq_matmat_mult(double* A1, double* A2, double* R, size_t n) {
  for (size_t i = 0; i < n; i++)
    for (size_t j = 0; j < n; j++)
      for (size_t k = 0; k < n; k++)
        R[i*n + j] += A1[i*n + k] * A2[k*n + i];
}
\end{lstlisting}

There are many ways we can parallelize this naive version. I tried three simple ways: inserting \texttt{pragma omp parallel for} before the outer loop to parallelize row computations, inserting it with a \texttt{collapse(2)} to allow OpenMP to split up the computation further, and finally doing both an outer collapse and an inner pragma with \texttt{reduction(+)} to parallelize the sum. Since they are almost identical, I won't duplicate the source code here, but they are available at \href{https://bitbucket.org/andross/cs205/src/master/hw1/p3/implementations.c}{https://bitbucket.org/andross/cs205/src/master/hw1/p3/implementations.c}. \\

One of the weaknesses of this approach in both the sequential \textit{and} parallel case is that it is inefficient with respect to data movement. We repeatedly access the same elements in the matrix but at different times (e.g. we read the first column of the second matrix $n$ times), which means we continually load data into the cache, eject it because we have to consider new data, but then have to fetch it again from lower cache levels or disk. This can lead to worse than $n^3$ performance. So instead we can loop through the matrix in blocks of rows and columns:

\begin{lstlisting}[language=C]
void tiled_par_matmat_mult(
  double* A1, double* A2, double* R, size_t n, size_t b) {
  #pragma omp parallel for collapse(2)
  for (size_t ii = 0; ii < n; ii+=b) {
    for (size_t jj = 0; jj < n; jj+=b) {
      for (size_t kk = 0; kk < n; kk+=b) {
        size_t imax = min(ii+b, n);
        size_t jmax = min(jj+b, n);
        size_t kmax = min(kk+b, n);
        for (size_t i=ii; i<imax; i++)
          for (size_t j=jj; j<jmax; j++)
            for(size_t k=kk; k<kmax; k++)
              R[i*n + j] += A1[i*n + k] * A2[k*n + j];
      }
    }
  }
}
\end{lstlisting}

This may not be the ideal tiling scheme, because we have no explicit concept of shared memory for the threads, but it definitely improved performance. To ensure this high performance, we need three times the square of the block size to be smaller than Odyssey's cache size, which I determined to be 2048 KB by examining \texttt{/proc/cpuinfo}. Since doubles are 8 bytes, we need $3(8b)^2 < 2048000$, which means $b$ must be less than or equal to $111$. In practice, I used $b=p$ (32 or 64) since I seemed to get the best results, perhaps because data fit in a higher cache level (for $b=p=32$, total data size is under 256 KB). I also benchmarked a sequential version of the tiled algorithm, which ended up being much faster than the normal sequential version for high $n$.

Finally, I figured out how to interface with \textit{dgemm}:

\begin{lstlisting}[language=C]
#include <gsl/gsl_blas.h>

void dgemm_matmat_mult(double* A1, double* A2, double* R, size_t n) {
   gsl_A_view A = gsl_A_view_array(A1, n, n);
   gsl_A_view B = gsl_A_view_array(A2, n, n);
   gsl_A_view C = gsl_A_view_array(R, n, n);
   gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
                  1.0, &A.A, &B.A,
                  0.0, &C.A);
}
\end{lstlisting}

This provided a strong benchmark against which to test everything else.\\

Here are results for $p=b=32$:

\begin{center}
\includegraphics[width=\textwidth]{matmat-runtimes.png}
\end{center}

Note that while \textit{dgemm} is fastest for small $n$, as we increase $n$ significantly the tiled parallel algorithm is faster. Note also that the runtimes for the tiled algorithms (both sequential and parallel) stay parallel to the $n^3$ trendline, whereas the non-tiled algorithm runtimes (including \textit{dgemm}) start to increase \textit{faster} than $n^3$ for large $n$, indicating poor data movement scaling characteristics. \\

To calculate speedup, I compared parallel runtimes to the sequential tiled runtimes, because using the naive sequential algorithm gave me speedups of over 100 for large $n$, which didn't seem like a fair comparison. All of the variants of non-tiled parallelism that I tried had similar speedup characteristics, which went down for large $n$ (as the data movement time, which is similar for both sequential and parallel algorithms, starts to dominate). The parallel tiled algorithm stays relatively steady at a speedup of around 20 (62.5\% efficiency). \\

Note that for all implementations, I made sure to pass a matrix of 0s as the result, passed in two full matrices of 1s as the inputs, and validated that the result elements were equal to $n$. It was challenging to learn how to use Odyssey and OpenMP from scratch and build all of the benchmarking and script-running code to finally obtain these results, but it was ultimately very useful!

\subsection*{Collaboration}

I didn't form a team to work on any of the problems, but I did discuss the problem set fairly extensively with Shawn Pan and Nathaniel Burbank (I think we ultimately took pretty different approaches, though).

\end{document}
